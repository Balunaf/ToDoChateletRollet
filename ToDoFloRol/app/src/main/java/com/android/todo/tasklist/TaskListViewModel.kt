package com.android.todo.tasklist

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.todo.network.Api
import com.android.todo.network.Api.tasksWebService
import com.android.todo.network.TasksRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class TaskListViewModel : ViewModel() { // Besoin d'une val pour tasksWebService ?
    private val repository = TasksRepository()
    private var _taskList = MutableStateFlow<List<Task>>(emptyList())
    public val taskList: StateFlow<List<Task>> = _taskList.asStateFlow()

    fun refresh() {
        viewModelScope.launch {
            val refresh = repository.refresh()
            if (refresh != null){
                _taskList.value = refresh
            }
            else {
                Log.d("TaskListViewModel", "Error : fetching null taskList")
            }
        }
    }

    fun delete(task: Task){
        var deleted = false
        viewModelScope.launch {
            deleted = repository.delete(task.id)
        }
        if(!deleted){
            Log.e("TasklistViewModel", "Error : deleting is impossible, the task doesn't exist")
        }
    }

    fun addOrEdit(task: Task) : Task?{
        var taskToReturn : Task? = null
        viewModelScope.launch {
            taskToReturn = repository.createOrUpdate(task)
        }
        if(taskToReturn == null){
            Log.e("TasklistViewModel", "Error while creating/updating task")
            return null
        }
        return taskToReturn
    }
}