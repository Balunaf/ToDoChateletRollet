package com.android.todo.tasklist

import java.io.Serializable
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable as Ser

@Ser
data class Task(
    @SerialName("id")
    var id : String,
    @SerialName("title")
    val title : String,
    @SerialName("description")
    val description : String = "Describe your task here !"
) : Serializable
