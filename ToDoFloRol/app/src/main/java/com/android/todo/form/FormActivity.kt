package com.android.todo.form

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.ImageButton
import com.android.todo.R
import com.android.todo.databinding.ActivityFormBinding
import com.android.todo.tasklist.Task
import java.util.*

class FormActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        val binding = ActivityFormBinding.inflate(layoutInflater)
        val imageButton = findViewById<ImageButton>(R.id.validateButton)
        var titleText = findViewById<EditText>(R.id.createTaskTitleText)
        var descriptionText = findViewById<EditText>(R.id.createTaskDescriptionText)
        val currentTask : Task? = intent.getSerializableExtra("task") as Task?
        if(currentTask != null){
            titleText.setText(currentTask.title)
            descriptionText.setText(currentTask.description)
        }
        imageButton.setOnClickListener{
            val newId = currentTask?.id ?: UUID.randomUUID().toString()
            val newTask = Task(newId, titleText.text.toString(), descriptionText.text.toString()) // TaskListFragment -->
            intent.putExtra("task", newTask)
            setResult(RESULT_OK, intent)
            finish()
        }
        when (intent?.action) {
            Intent.ACTION_SEND -> {
                if ("text/plain" == intent.type) {
                    handleSendText(intent, descriptionText) // Handle text being sent
                }
            }
        }
    }
    private fun handleSendText(intent: Intent, editText: EditText) {
        intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
            editText.setText(intent.getStringExtra(Intent.EXTRA_TEXT))
        }
    }

}