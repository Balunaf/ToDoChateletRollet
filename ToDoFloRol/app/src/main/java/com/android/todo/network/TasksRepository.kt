package com.android.todo.network

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.android.todo.tasklist.Task
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class TasksRepository {
    private val webService = Api.tasksWebService

    suspend fun refresh() : List<Task>? {
        val tasksResponse = webService.getTasks()
            // À la ligne suivante, on a reçu la réponse de l'API:
        if (!tasksResponse.isSuccessful) {
            Log.e("TasksRepository", "Error while fecthing tasks : ${tasksResponse.message()}")
            return null
        }// NO ERROR
        return tasksResponse.body()
    }

    suspend fun createOrUpdate(task: Task) : Task?{
        var foundTask = false;
        val tasksResponse = webService.getTasks()
        tasksResponse.body()?.forEach{
            if(task.id == it.id){
                foundTask = true;
            }
        }
        val response = when (foundTask){
            true -> Api.tasksWebService.update(task)// update
            false -> Api.tasksWebService.create(task)// create
        }
        if (!response.isSuccessful) {
            return null
        }
        return response.body()
    }

    suspend fun delete(id: String) : Boolean{
        val deleteResponse = Api.tasksWebService.delete(id)
        if (!deleteResponse.isSuccessful) {
            Log.e("TasksRepository", "Error while fecthing tasks : ${deleteResponse.message()}")
            return false
        }
        return true
    }

    // Ces deux variables encapsulent la même donnée:
    // [_taskList] est modifiable mais privée donc inaccessible à l'extérieur de cette classe



}