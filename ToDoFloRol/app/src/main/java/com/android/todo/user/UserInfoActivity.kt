package com.android.todo.user

import android.Manifest
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.Image
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.core.content.PermissionChecker
import androidx.core.net.toUri
import androidx.lifecycle.lifecycleScope
import coil.load
import com.android.todo.R
import com.android.todo.databinding.ActivityMainBinding
import com.android.todo.databinding.ActivityUserInfoBinding
import com.android.todo.databinding.FragmentTaskListBinding
import com.android.todo.network.Api
import com.google.android.material.snackbar.Snackbar
import com.google.modernstorage.mediastore.FileType
import com.google.modernstorage.mediastore.MediaStoreRepository
import com.google.modernstorage.mediastore.SharedPrimary
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.util.*

class UserInfoActivity : AppCompatActivity() {
    val mediaStore by lazy { MediaStoreRepository(this) }
    var imageView : ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_info)
        imageView = findViewById(R.id.avatarImageView)
        lifecycleScope.launch{
            val userInfo = Api.userWebService.getInfo().body()
            imageView?.load(userInfo?.avatar){
                error(R.drawable.ic_launcher_background)
            }
        }
        lifecycleScope.launchWhenCreated {
            photoUri = mediaStore.createMediaUri(
                filename = "picture-${UUID.randomUUID()}.jpg",
                type = FileType.IMAGE,
                location = SharedPrimary
            ).getOrThrow()
        }
        findViewById<Button>(R.id.take_picture_button).setOnClickListener{
            cameraPermissionLauncher.launch("camera/*")
        }
        findViewById<Button>(R.id.upload_image_button).setOnClickListener{
            galleryLauncher.launch("image/*")
        }
    }

    private val cameraPermissionLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { accepted ->
            if (accepted) launchCamera()
            else launchCameraWithPermission()
        }

    private fun launchCameraWithPermission() {
        val camPermission = Manifest.permission.CAMERA
        val permissionStatus = PermissionChecker.checkSelfPermission(applicationContext, camPermission)
        val isAlreadyAccepted = permissionStatus == PackageManager.PERMISSION_GRANTED
        val isExplanationNeeded = shouldShowRequestPermissionRationale(camPermission)
        when {
            isAlreadyAccepted -> launchCamera() // lancer l'action souhaitée
            isExplanationNeeded -> showExplanation()// afficher une explication
            else -> launchAppSettings() // lancer la demande de permission
        }
    }

    private fun launchAppSettings() {
        // Cet intent permet d'ouvrir les paramètres de l'app (pour modifier les permissions déjà refusées par ex)
        val intent = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.fromParts("package", "com.android.todo", null)
        )
        // ici pas besoin de vérifier avant car on vise un écran système:
        startActivity(intent)
    }

    private fun showExplanation() {
        // ici on construit une pop-up système (Dialog) pour expliquer la nécessité de la demande de permission
        AlertDialog.Builder(applicationContext)
            .setMessage("🥺 On a besoin de la caméra, vraiment! 👉👈")
            .setPositiveButton("Bon, ok") { _, _ -> launchAppSettings()/* ouvrir les paramètres de l'app */ }
            .setNegativeButton("Nope") { dialog, _ -> dialog.dismiss() }
            .show()
    }

    private fun convert(uri: Uri): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            name = "avatar",
            filename = "temp.jpeg",
            body = contentResolver.openInputStream(uri)!!.readBytes().toRequestBody()
        )
    }

    private fun handleImage() {
        imageView?.setImageURI(photoUri)
        lifecycleScope.launch{
            Api.userWebService.updateAvatar(convert(photoUri))
        }
    }

    private val cameraLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) { accepted ->
        val view = findViewById<ImageView>(R.id.image_view)
        view.setImageURI(photoUri)
        if(accepted) handleImage()
        else Snackbar.make(view, "Échec !", Snackbar.LENGTH_LONG).show()
    }

    // use
    private fun launchCamera() {
        cameraLauncher.launch(photoUri)
    }
    private lateinit var photoUri : Uri

    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.GetContent()) {
        val view = findViewById<ImageView>(R.id.image_view)
        view.setImageURI(it)
        if (it != null) {
            photoUri = it
        }
        handleImage()

    }


    override fun getContentResolver(): ContentResolver {
        return super.getContentResolver()
    }


}