package com.android.todo.tasklist

import android.os.Parcel
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.android.todo.R
import com.android.todo.databinding.ItemTaskBinding
import com.android.todo.tasklist.TaskListAdapter.TaskViewHolder


object TaskDiffCallback : DiffUtil.ItemCallback<Task>() {
    override fun areItemsTheSame(oldItem: Task, newItem: Task) =
        oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: Task, newItem: Task) =
        oldItem.title == newItem.title &&
                oldItem.description == newItem.description
}

interface TaskListListener{
    fun onClickDelete(task: Task)
    fun onClickModify(task: Task)
    fun onClickTask(task: Task)
}

class TaskListAdapter(val listener: TaskListListener) : ListAdapter<Task, TaskViewHolder>(TaskDiffCallback) {
    // on utilise `inner` ici afin d'avoir accès aux propriétés de l'adapter directement
    inner class TaskViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val binding = ItemTaskBinding.bind(itemView)
        fun bind(task: Task) {
            binding.taskTitle.text = task.title
            binding.taskDescription.text = task.description
            binding.deleteButton.setOnClickListener{
                listener.onClickDelete(task)
                submitList(currentList - task)
            }
            binding.modifyButton.setOnClickListener{
                listener.onClickModify(task)
            }
            binding.taskTitle.setOnClickListener{
                listener.onClickTask(task)
            }
            binding.taskDescription.setOnClickListener{
                listener.onClickTask(task)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TaskViewHolder {
        return TaskViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_task, parent, false))
    }

    override fun onBindViewHolder(holder: TaskViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}
