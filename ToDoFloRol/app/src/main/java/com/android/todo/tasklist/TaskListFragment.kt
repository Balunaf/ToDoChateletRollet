package com.android.todo.tasklist

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.ContentResolver
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.core.content.PermissionChecker
import androidx.core.net.toUri
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.android.todo.R
import com.android.todo.databinding.FragmentTaskListBinding
import com.android.todo.form.FormActivity
import com.android.todo.network.Api
import com.android.todo.network.TasksRepository
import kotlinx.coroutines.launch
import java.io.Serializable
import java.util.*
import coil.*
import coil.request.ImageRequest
import coil.transform.CircleCropTransformation
import com.android.todo.user.UserInfoActivity
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

//QUESTIONS : TP4 accompli ? Pourquoi mon bouton fonctionne pas ?

class TaskListFragment : Fragment() {

    private var binding: FragmentTaskListBinding? = null
    private val Binding get() = binding!!
    val adapterListener = object : TaskListListener{
        override fun onClickDelete(task: Task) {
            lifecycleScope.launch{
                viewModel.delete(task)
            }
        }

        override fun onClickModify(task: Task) {
            val intent = Intent(context, FormActivity::class.java)
            intent.putExtra("task", task)
            formLauncher.launch(intent)
        }

        override fun onClickTask(task: Task) {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, task.title + "\n\n" + task.description)
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(shareIntent)
        }
    }
    private val adapter = TaskListAdapter(adapterListener)
    private val Adapter get() = adapter!!
    // On récupère une instance de ViewModel
    private val viewModel: TaskListViewModel by viewModels()




    val formLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
        result->
        val task = result.data?.getSerializableExtra("task") as? Task
        lifecycleScope.launch {
            if (task != null) {
                viewModel.addOrEdit(task)
            }
        }

    }



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentTaskListBinding.inflate(inflater,container,false)
        return Binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        Log.d("TLF", "RESUME")
        val userInfoTextView = view?.findViewById<TextView>(R.id.userInfoTextView)
        val avatarView = view?.findViewById<ImageView>(R.id.avatarImageView)
        lifecycleScope.launch {
            val userInfo = Api.userWebService.getInfo().body()!!
            userInfoTextView?.text = "${userInfo.firstName} ${userInfo.lastName}"
            avatarView?.load(userInfo.avatar){
                crossfade(true)
                transformations(CircleCropTransformation())
            }
            Adapter.submitList(viewModel.taskList.value)
            viewModel.refresh()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Binding.recyclerView.layoutManager = LinearLayoutManager(context)
        Binding.recyclerView.adapter = adapter
        Binding.floatingActionButton.setOnClickListener{
            newTask()
        }
        when (activity?.intent?.action) {
            Intent.ACTION_SEND -> {
                if ("text/plain" == activity?.intent?.type) {
                    newTask(activity?.intent) // Handle text being sent
                }
            }
        }
        //Binding.avatarImageView.bringToFront()
        Binding.avatarImageView.setOnClickListener{
            val intent = Intent(context, UserInfoActivity::class.java)
            formLauncher.launch(intent)
        }
        lifecycleScope.launch { // on lance une coroutine car `collect` est `suspend`
            viewModel.taskList.collect { newList ->
                Adapter.submitList(newList)
            }
        }
    }

    private fun newTask(intent: Intent? = null) { // SEND MESSAGE NEW TASK (FROM UI OR SHARE)
        val newIntent = Intent(context, FormActivity::class.java)
        if (intent != null){
            newIntent.putExtra("task",
                intent.getStringExtra(Intent.EXTRA_TEXT)?.let {
                    Task(UUID.randomUUID().toString(), "Title",
                        it
                    )
                })
        }
        formLauncher.launch(newIntent) // --> FormActivity
    }
}